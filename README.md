## Description

NxstJS CRUD Products frontend app

### Considerations

This app runs in port 3000, so make sure you don't have any other app running in that port.

## Installation

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

