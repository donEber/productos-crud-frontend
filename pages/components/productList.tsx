import { useState, useEffect } from 'react'
import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import DialogContentText from '@mui/material/DialogContentText';
import Dialog from '@mui/material/Dialog';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';

//interface of product
interface Product {
  id?: number | null,
  name: string,
  price: number,
  quantity: number,
  date_registration?: Date,
  date_update?: Date
}

export default function ProductList(props) {
  const [data, setData] = useState(null)
  const [isLoading, setLoading] = useState(false)
  const [open, setOpen] = useState(false);
  const [formData, setFormData] = useState({
    id: null,
    name: '',
    price: 0,
    quantity: 0,
  });
  const [createOrEditProduct, setCreateOrEditProduct] = useState(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleClickOpen = (createOrEditOps:boolean) => {
    setCreateOrEditProduct(createOrEditOps)
    setOpen(true);
  };

  const handleDialogAction = () => {
    if( createOrEditProduct)
      createProduct()
    else
      editProduct(formData.id, formData.name, formData.price, formData.quantity)
    setOpen(false)
  };

  useEffect(() => {
    setLoading(true)
    fetch('http://localhost:3001/products')
      .then((res) => res.json())
      .then((data) => {
        setData(data)
        setLoading(false)
      })
      .catch((err) => {
        console.error(err)
        setLoading(false)
      })
  }, [])

  function deleteProduct(id: number) {
    fetch(`http://localhost:3001/products/${id}`, {
      method: 'DELETE'
    })
      .then(res => res.json())
      .then(res => {
        const newData = data.filter(item => item.id !== id)
        setData(newData)
      })
  }

  function createProduct() {
    const newProduct: Product = {
      name: formData.name,
      price: formData.price,
      quantity: formData.quantity
    }
    fetch(`http://localhost:3001/products/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newProduct),
    })
      .then(res => res.json())
      .then(res => {
        setData([...data, res])
      })
      .catch(err => console.error(err))
  }

  function editProduct(id: number, name: string, price: number, quantity: number) {
    const productToEdit: Product = {
      name: name,
      price: price,
      quantity: quantity
    }
    // fetch to edit product using PATCH http method
    fetch(`http://localhost:3001/products/${id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(productToEdit),
    })
      .then(res => res.json())
      .then(res => {
        const newData = data.map(item => item.id === id ? res : item)
        setData(newData)
      })
      .catch(err => console.error(err))

  }


  if (isLoading) return <p>Loading...</p>
  if (!data) return <p>No profile data</p>

  return (
    <section>
      <Button onClick={ () => {handleClickOpen(true)}}>
        <AddIcon />
        Create Product
      </Button>
      <h1>Lista de Productos</h1>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>#</TableCell>
              <TableCell>Nombre</TableCell>
              <TableCell align="right">Precio</TableCell>
              <TableCell align="right">Cantidad</TableCell>
              <TableCell align="right">Actualizado</TableCell>
              <TableCell align="right">Regitrado</TableCell>
              <TableCell align="right">Acciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row, index: number) => (
              <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {index + 1}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.price}</TableCell>
                <TableCell align="right">{row.quantity}</TableCell>
                <TableCell align="right">{(new Date(row.date_update).toLocaleTimeString()).concat(' ',new Date(row.date_update).toLocaleDateString())}</TableCell>
                <TableCell align="right">{(new Date(row.date_registration).toLocaleTimeString()).concat(' ',new Date(row.date_registration).toLocaleDateString())}</TableCell>
                <TableCell align="right">
                  <IconButton aria-label="delete" onClick={() => deleteProduct(row.id)} >
                    <DeleteIcon />
                  </IconButton>
                  <IconButton aria-label="edit" onClick={() => { handleClickOpen(false); setFormData({ id: row.id, name: row.name, price: row.price, quantity: row.quantity }) }} >
                    <EditIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {createOrEditProduct ? "Crear Producto" : "Editar Producto"}
        </DialogTitle>
        <DialogContent>
          <Box
            component="form"
            sx={{
              '& > :not(style)': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField id="standard-basic" value={formData.name} name="name" label="Nombre" variant="standard" onChange={handleInputChange} />
            <TextField id="standard-basic" value={formData.price} name="price" label="Precio" variant="standard" type="number" onChange={handleInputChange} />
            <TextField id="standard-basic" value={formData.quantity} name="quantity" label="Cantidad" variant="standard" type="number" onChange={handleInputChange} />
            <DialogActions>
              <Button onClick={() => { setOpen(false); setFormData({name:"", price: 0, quantity: 0}) }}>
                Cancelar
              </Button>
              <Button onClick={() => handleDialogAction()}>
                { createOrEditProduct? 'Crear': 'Editar'}
              </Button>
            </DialogActions>
          </Box>

        </DialogContent>
      </Dialog>
    </section>
  )
}